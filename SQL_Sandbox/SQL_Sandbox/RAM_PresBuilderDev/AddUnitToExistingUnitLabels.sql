﻿
DECLARE @GroupID VARCHAR(100)
, @SubGroupID VARCHAR(100)
, @datasetID INT
, @unitProcessType VARCHAR(100)


DECLARE c_outer CURSOR READ_ONLY STATIC LOCAL
FOR
 
	SELECT dlu1.DatasetID, ans.strValue
	FROM Dataset_LU dlu1
	INNER JOIN Dataset_LU dlu2 ON dlu1.ParentID = dlu2.DatasetID
		AND dlu2.DataLevel = 'SITE'
		AND dlu2.ParentID = 7289
	INNER JOIN Answers ans ON dlu1.DatasetID = ans.DatasetID
		AND ans.PropertyNo = 325
	WHERE dlu1.DataLevel = 'UNIT'
 
OPEN c_outer
FETCH NEXT FROM c_outer
INTO @datasetID, @unitProcessType
 
WHILE @@FETCH_STATUS <> -1 
    BEGIN
     
	 PRINT CONVERT(VARCHAR(10),@datasetID) + '	' + @unitProcessType

		DECLARE c_inner CURSOR READ_ONLY STATIC LOCAL
		FOR
 
			     SELECT 
					   DISTINCT
					   [GroupID]
					  ,[SubgroupID]
				  FROM [RAM].[Output].[GroupUnits]
				  WHERE 
				  (
				  GroupID like 'RAM15%'
				  AND SubGroupID = @unitProcessType
				  )
				  OR
				  (
						GroupID like 'RAM15%' + @unitProcessType
				  )
 
		OPEN c_inner
		FETCH NEXT FROM c_inner
		INTO @GroupID, @SubGroupID
 
		WHILE @@FETCH_STATUS <> -1 
			BEGIN

				Print '		' + @GroupID + '	' + @SubGroupID

				INSERT [RAM].[Output].[GroupUnits]
				SELECT  @GroupID
						, @SubgroupID
						, @datasetID

		    FETCH NEXT FROM c_inner
			INTO @GroupID, @SubGroupID
			END
     
		CLOSE c_inner
		DEALLOCATE c_inner
         
         
    FETCH NEXT FROM c_outer
    INTO @datasetID, @unitProcessType
    END
     
CLOSE c_outer
DEALLOCATE c_outer








--SELECT dlu1.DatasetID, ans.strValue
--FROM Dataset_LU dlu1
--INNER JOIN Dataset_LU dlu2 ON dlu1.ParentID = dlu2.DatasetID
--	AND dlu2.DataLevel = 'SITE'
--	AND dlu2.ParentID = 7289
--INNER JOIN Answers ans ON dlu1.DatasetID = ans.DatasetID
--	AND ans.PropertyNo = 325
--WHERE dlu1.DataLevel = 'UNIT'





----print @GroupID + '	' + @SubGroupID

----INSERT [RAM].[Output].[GroupUnits]
--SELECT @GroupID
--, @SubGroupID
--, CAST(datasetid AS VARCHAR(25))
--FROM Dataset_LU
--WHERE Deleted = 0
--	AND ParentID IN (
--		SELECT DatasetID
--		FROM Dataset_LU
--		WHERE ParentID IN (
--				SELECT DatasetID
--				FROM Dataset_LU
--				WHERE FacilitySID = 'INEOS15'
--				)
--		)



  --   SELECT 
		--   DISTINCT
		--   [GroupID]
		--  ,[SubgroupID]
		--  --,[DatasetID]
	 -- FROM [RAM].[Output].[GroupUnits]
	 -- --WHERE GroupID like 'RAM15%'
	 -- WHERE 
	 -- (
	 -- GroupID like 'RAM15%'
	 -- AND SubGroupID in
	 -- (
		--'C_PLAST'
		--,'OthChem'
		--,'PM_OLE'
		--,'Utilities'
		----,'ALL'
	 -- ))
	 -- OR
	 -- (
		--	GroupID like 'RAM15%C_PLAST'
		--  OR GroupID like 'RAM15%OthChem'
		--  OR GroupID like 'RAM15%PM_OLE'
		--  OR GroupID like 'RAM15%Utilities'
		--  OR GroupID like 'RAM15%ALL'
	 -- )